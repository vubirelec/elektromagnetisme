This repository contains information on the 'Elektromagnetisme' course. 


PLEASE KEEP THIS REPOSITORY UPDATED FOR FUTURE ASSISTENTS!


This folder contains:

- the theory course (EMBOOK.pdf)

- an impedance smith chart (Smith Chart BWz.pdf)

- an impedance and admittance smith chart (Smith Chart Color.pdf)

- extra information on the smith chart

- the lab assigments

- old exams 

